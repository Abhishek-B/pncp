function [ X, Y, Z ] = Rstep1_1(n,m)
% Step1_1  - Generates random points Z_{i} (for the algorithm 4.1) using
%           Gaussian distribution. See the function file for more details.
% Inputs:
%        n,m - Integers which determine the size of the spaces used.
%
% Outputs:
%         Z - A matrix which contains (n-1)*(m-1) random points Z_{i} on
%             the Segre Variety as column vectors.

%{
The first two matrices store the individual xi yi points. 
The matrix Z stores all the points zi, formed from the 
kronecker products of xi and yi.

We use here the built in matlab function kron. 
The function randn is used to get random values with Gaussian
dist. This should be changed according to how you want the points
sampled.
%}

e=(n-1)*(m-1);

X = randi([-1,1],n,e+1); %rand(n,e+1);
Y = randi([-1,1],m,e+1); %rand(m,e+1); 
Z = zeros(n*m,e+1);
for i=1:e+1
    Z(:,i) = kron(X(:,i),Y(:,i));
end

end

