%% Algorithm tests

% setting path variables.
p = cell(5,5);

cd ..
cd EXPTS
cd '(3,3)'
p{3,3} = pwd;
cd ..
cd '(3,4)'
p{3,4} = pwd;
cd ..
cd '(3,5)'
p{3,5} = pwd;
cd ..
cd '(4,4)'
p{4,4} = pwd;
cd ..
cd '(4,5)'
p{4,5} = pwd;
cd ..
cd ..
cd 'Code'


%% Testing the CNR with mosek

ntrials = 50;
solver = 'mosek';
n=3;
for m=3:5
    
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('||%11s %40s %15s|| \n',...
        repmat(' ',1,11),strcat('Testing Examples of size (',int2str(n),',',int2str(m),') -- Mosek') ,...
        repmat(' ',1,15))
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('%9s|%11s|%9s|%9s|%9s|%9s|%9s \n',...
        ' Test# ', ' Algorithm ', ' Time   ', ' Outcome ',...
        ' Delta  ', ' Degree ', ' Residual ')
    fprintf('%72s \n', repmat('=', 1,72))
    
    pth = p{n,m};
    
    for k=1:ntrials
        
        % Loading Saved Data
        filename = strcat('Data(',int2str(n),int2str(m),')',int2str(k),'.mat');
        data = fullfile(pth, filename);
        S = load(data);
        
        % Solving the SDP
        
        tic;
        testCNR = 0; delCNR = 0; degCNR=1;
        while testCNR == 0 && degCNR<3
            [testCNR, delCNR, resCNR] = step3(n,m,S.v,[S.v0 S.V],degCNR,solver);
            if testCNR==0
                degCNR = degCNR+1;
            end
        end
        tCNR = toc;
        
        fprintf('%9d|%11s|%9.4f|%9s|%9.6f|%9s|%9.8f \n', k, 'Step 3', tCNR, strcat(int2str(testCNR),"   "), delCNR, strcat(int2str(degCNR),"   "), resCNR)
        
        
%         tic;
%         test2 = 0; del2 = 1; degree2 = 1;
%         while test2 == 0 && del2 > 2^(-7) && degree2 < 3
%             [test2, res2] = step3_HilbertB(n,m,S.v,[S.v0 S.V],degree2,del2,solver);
%             if test2 ==0 && del2 > 2^(-6)
%                 del2 = del2/2 ;
%             elseif test2==0 && del2 == 2^(-6)
%                 del2 = 1;
%                 degree2 = degree2+1;
%             end
%         end
%         t2 = toc;
%         
%         fprintf('%9s|%11s|%9.4f|%9s|%9.6f|%9s|%9.8f \n', strcat('(',int2str(n),',',int2str(m),')'), 'Hilbert+B', t2, strcat(int2str(test2),"   "), del2, strcat(int2str(degree2),"   "), res2)
%         

        fprintf('%72s \n',repmat('-',1,72))
        
        %storage
        
        filename = strcat('Results_CNR(',int2str(n),int2str(m),')',int2str(k),'.mat');
        datafile = fullfile(pth, filename);
        save(datafile,'testCNR','tCNR', 'delCNR', 'degCNR','resCNR')
        clear S;
    end
end

%% Testing the Hilbert relaxation with mosek

ntrials = 50;
solver = 'mosek';
n=3;
for m=n:5
    
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('||%11s %40s %15s|| \n',...
        repmat(' ',1,11),strcat('Testing Examples of size (',int2str(n),',',int2str(m),') -- Mosek') ,...
        repmat(' ',1,15))
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('%9s|%11s|%9s|%9s|%9s|%9s|%9s \n',...
        ' Test# ', ' Algorithm ', ' Time   ', ' Outcome ',...
        ' Delta  ', ' Degree ', ' Residual ')
    fprintf('%72s \n', repmat('=', 1,72))
    
    pth = p{n,m};
    
    for k=1:ntrials
        
        % Loading Saved Data
        filename = strcat('Data(',int2str(n),int2str(m),')',int2str(k),'.mat');
        data = fullfile(pth, filename);
        S = load(data);
        
        % Solving the SDP     
        
        tic;
        testHIL = 0; delHIL = 1; degHIL = 1;
        while testHIL == 0 && delHIL > 2^(-7) && degHIL < 3
            [testHIL, resHIL] = step3_HilbertB(n,m,S.v,[S.v0 S.V],degHIL,delHIL,solver);
            if testHIL ==0 && delHIL > 2^(-6)
                delHIL = delHIL/2 ;
            elseif testHIL==0 && delHIL == 2^(-6)
                delHIL = 1;
                degHIL = degHIL+1;
            end
        end
        tHIL = toc;
        
        fprintf('%9d|%11s|%9.4f|%9s|%9.6f|%9s|%9.8f \n', k, 'Hilbert+B', tHIL, strcat(int2str(testHIL),"   "), delHIL, strcat(int2str(degHIL),"   "), resHIL)
        

        fprintf('%72s \n',repmat('-',1,72))
        
        %storage
        
        filename = strcat('Results_HIL(',int2str(n),int2str(m),')',int2str(k),'.mat');
        datafile = fullfile(pth, filename);
        save(datafile,'testHIL','tHIL', 'delHIL', 'degHIL','resHIL')
        clear S;
    end
end

%% Testing the KKT-B relaxation with mosek

ntrials = 50;
solver = 'mosek';
n=3;
for m=n:5
    
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('||%11s %40s %15s|| \n',...
        repmat(' ',1,11),strcat('Testing Examples of size (',int2str(n),',',int2str(m),') -- Mosek') ,...
        repmat(' ',1,15))
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('%9s|%11s|%9s|%9s|%9s|%9s|%9s \n',...
        ' Test# ', ' Algorithm ', ' Time   ', ' Outcome ',...
        ' Delta  ', ' Degree ', ' Residual ')
    fprintf('%72s \n', repmat('=', 1,72))
    
    pth = p{n,m};
    
    for k=1:ntrials
        
        % Loading Saved Data
        filename = strcat('Data(',int2str(n),int2str(m),')',int2str(k),'.mat');
        data = fullfile(pth, filename);
        S = load(data);
        
        % Solving the SDP     
        
        tic;
        testKKTB = 0; delKKTB = 1; degKKTB = 1;
        while testKKTB == 0 && delKKTB > 2^(-7) && degKKTB < 3
            [testKKTB, resKKTB] = step3_KKTB(n,m,S.v,[S.v0 S.V],degKKTB,delKKTB,solver);
            if testKKTB ==0 && delKKTB > 2^(-6)
                delKKTB = delKKTB/2 ;
            elseif testKKTB==0 && delKKTB == 2^(-6)
                delKKTB = 1;
                degKKTB = degKKTB+1;
            end
        end
        tKKTB = toc;
        
        fprintf('%9d|%11s|%9.4f|%9s|%9.6f|%9s|%9.8f \n', k, 'KKT-B', tKKTB, strcat(int2str(testKKTB),"   "), delKKTB, strcat(int2str(degKKTB),"   "), resKKTB)
        

        fprintf('%72s \n',repmat('-',1,72))
        
        %storage
        
        filename = strcat('Results_KKTB(',int2str(n),int2str(m),')',int2str(k),'.mat');
        datafile = fullfile(pth, filename);
        save(datafile,'testKKTB','tKKTB', 'delKKTB', 'degKKTB','resKKTB')
        clear S;
    end
end

%% Testing the Jacobian relaxation with mosek

ntrials = 50;
solver = 'mosek';
n=3;
for m=n:5
    
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('||%11s %40s %15s|| \n',...
        repmat(' ',1,11),strcat('Testing Examples of size (',int2str(n),',',int2str(m),') -- Mosek') ,...
        repmat(' ',1,15))
    fprintf('%72s \n', repmat('=', 1,72))
    fprintf('%9s|%11s|%9s|%9s|%9s|%9s|%9s \n',...
        ' Test# ', ' Algorithm ', ' Time   ', ' Outcome ',...
        ' Delta  ', ' Degree ', ' Residual ')
    fprintf('%72s \n', repmat('=', 1,72))
    
    pth = p{n,m};
    
    for k=1:ntrials
        
        % Loading Saved Data
        filename = strcat('Data(',int2str(n),int2str(m),')',int2str(k),'.mat');
        data = fullfile(pth, filename);
        S = load(data);
        
        % Solving the SDP     
        
        tic;
        testJAC = 0; delJAC = 1; degJAC = 1;
        while testJAC == 0 && delJAC > 2^(-7) && degJAC < 3
            [testJAC, resJAC] = step3_jac(n,m,S.v,[S.v0 S.V],degJAC,delJAC,solver);
            if testJAC == 0 && delJAC > 2^(-6)
                delJAC = delJAC/2 ;
            elseif testJAC == 0 && delJAC == 2^(-6)
                delJAC = 1;
                degJAC = degJAC+1;
            end
        end
        tJAC = toc;
        
        fprintf('%9d|%11s|%9.4f|%9s|%9.6f|%9s|%9.8f \n', k, 'Jacobian', tJAC, strcat(int2str(testJAC),"   "), delJAC, strcat(int2str(degJAC),"   "), resJAC)
        

        fprintf('%72s \n',repmat('-',1,72))
        
        %storage
        
        filename = strcat('Results_JAC(',int2str(n),int2str(m),')',int2str(k),'.mat');
        datafile = fullfile(pth, filename);
        save(datafile,'testJAC','tJAC', 'delJAC', 'degJAC','resJAC')
        clear S;
    end
end