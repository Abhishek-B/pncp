
%% Generating sample.

%{
First example was with hilberts method. 
Second example is done with Igor's denominator needing delta to be bigger
than 1. 
Both methods are fine, but Igor's denominator is easier to analyse.

Also tested with Sedumi, but that gives no results.
%}



n=3;
m=3;
l=1;
%del = 1/100;
solver = 'mosek';


example = 0;
count = 0;

while example==0 && count<1001
    disp(count)
    sos = 1;

    while sos==1                                                                    % This loop will keep generating forms until it 
        [X,Y,Z] = Rstep1_1(n,m);                                                          % finds one which is not a SOS for any given delta.
        V = Rstep1_2(n,m,Z);                                                        % This is only a numerical test and any final 
        v0 = Rstep1_3(n,m,Z);                                                       % results should be manually checked. 
        V = [v0, V];                                                                %
                                                                                    % I'm currently unsure of how to get an exact 
        W = Rstep2_1(n,m,Z);                                                        % certificate for 'non-SOS', but for now this will
        v = Rstep2_2(n,m, Z, W, v0, V);                                             % do.
                                                                                    % 
        sos = isSOS(n,m,v,V, solver);                                               %
    end                                                                             %

    disp("Found non SOS form, testing non-negativity")

    [nng, del, res, G] = Rstep3(n,m,v,V,l, solver);                                 % Numerical testing of non-negativity

    if nng==1                                                                       % If the numerical test implies non-negativity            
        disp("Non SOS form which is non-negative")
        example = 1;                                                                % we set example to 1 indicating a rational example
        pathname = pwd;                                                             % has been found.
        filename = 'RIgor3.mat';                                             %
        datafile = fullfile(pathname, filename);                                    % The file is saved in the current directory with all
        save(datafile,'n','m','l','del','X','Y','Z','V','v','sos','nng','res', 'G');             % relevant info to reproduce results.
    else                                                                            %
        disp("negative form")                                                       %
    end                                                                             % Reproducability is however machine dependent as was found in 'debug'.
    
    count = count+1;
end


%% Analysing Sample.

X=sdpvar(m,1);
Y=sdpvar(n,1);

f = double(v')*kron(kron(X,Y),kron(X,Y));
h_linforms = double(V')*kron(X,Y);
h_sos = h_linforms'*h_linforms;
