%% KKT Algorithm tests

% setting path variables.
p = cell(5,5);

cd ..
cd Experiments
cd '(3,3)'
p{3,3} = pwd;
cd ..
cd '(3,4)'
p{3,4} = pwd;
cd ..
cd '(3,5)'
p{3,5} = pwd;
cd ..
cd '(4,4)'
p{4,4} = pwd;
cd ..
cd '(4,5)'
p{4,5} = pwd;
cd ..
cd ..
cd 'Abhishek''s code'

%% Tests Mosek

ntrials = 50;
solver = 'mosek';

for n=3:4
    for m=n:5
        
        fprintf('%72s \n', repmat('=', 1,72))
        fprintf('||%11s %40s %15s|| \n',...
            repmat(' ',1,11),strcat('Testing Examples of size (',int2str(n),',',int2str(m),') -- Mosek') ,...
            repmat(' ',1,15))
        fprintf('%72s \n', repmat('=', 1,72))
        fprintf('%9s|%11s|%9s|%9s|%9s|%9s|%9s \n',...
            ' Test# ', ' Algorithm ', ' Time   ', ' Outcome ',...
            ' Delta  ', ' Degree ', ' Residual ')
        fprintf('%72s \n', repmat('=', 1,72))
        
        pth = p{n,m}; 
        
        for k=1:ntrials
            
            % Loading Saved Data
            %pathname = fileparts('');
            filename = strcat('TestData(',int2str(n),',',int2str(m),')',int2str(k),'.mat');
            data = fullfile(pth, filename);
            load(data);
            
            % Solving the SDP
            
            tic;
            test = 0; del = 1; degree=1;
            while test == 0 && degree<4
                [test, res, cfg, cfc] = step3_KKTB(n,m,v,[v0 V],degree,del, solver);
                if test==0 && del>2^(-6)
                    del = del/2;
                elseif test==0 && del==2^(-6)
                    degree = degree+1;
                    del = 1;  
                end
            end
            time = toc;
            
            fprintf('%9s|%11s|%9.4f|%9s|%9.6f|%9s|%9.8f \n', strcat(int2str(k),"    "), 'KKT_B   ', time, strcat(int2str(test),"   "), del, strcat(int2str(degree),"   "), res)
            
            Success = [test];
            timing = [time];
            DEL = [del];
            Degrees = [degree];
            Res = [res];
            
            %storage

            filename = strcat('Results_KKT(',int2str(n),int2str(m),')',int2str(k),'.mat');
            datafile = fullfile(pth, filename);
            save(datafile,'Success','timing', 'DEL', 'Degrees','Res')
        end
    end
end
%}
