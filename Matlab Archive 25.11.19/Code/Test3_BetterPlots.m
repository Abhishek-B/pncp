%{
Plots of the results of the algorithms tested
%}
set(0,'DefaultFigureWindowStyle','docked')

% setting path variables.
p = cell(5,5);

cd ..
cd EXPTS
cd '(3,3)'
p{3,3} = pwd;
cd ..
cd '(3,4)'
p{3,4} = pwd;
cd ..
cd '(3,5)'
p{3,5} = pwd;
cd ..
cd '(4,4)'
p{4,4} = pwd;
cd ..
cd '(4,5)'
p{4,5} = pwd;
cd ..
cd ..
cd 'Code'
%% Timing Plots Hilbert


n=3;
m=3;
pathname = p{n,m};

SuccessHIL33 = [];
TimeHIL33 = [];
ResHIL33 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_HIL(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessHIL33 = [SuccessHIL33; testHIL];
    TimeHIL33 = [TimeHIL33; tHIL];
    ResHIL33 = [ResHIL33; resHIL];
end

avgtHIL33 = mean(TimeHIL33, 1);
avgsHIL33 = mean(SuccessHIL33, 1);
avgrHIL33 = mean(ResHIL33,1);


n=3;
m=4;
pathname = p{n,m};

SuccessHIL34 = [];
TimeHIL34 = [];
ResHIL34 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_HIL(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessHIL34 = [SuccessHIL34; testHIL];
    TimeHIL34 = [TimeHIL34; tHIL];
    ResHIL34 = [ResHIL34; resHIL];
end

avgtHIL34 = mean(TimeHIL34, 1);
avgsHIL34 = mean(SuccessHIL34, 1);
avgrHIL34 = mean(ResHIL34,1);



n=3;
m=5;
pathname = p{n,m};

SuccessHIL35 = [];
TimeHIL35 = [];
ResHIL35 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_HIL(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessHIL35 = [SuccessHIL35; testHIL];
    TimeHIL35 = [TimeHIL35; tHIL];
    ResHIL35 = [ResHIL35; resHIL];
end

avgtHIL35 = mean(TimeHIL35, 1);
avgsHIL35 = mean(SuccessHIL35, 1);
avgrHIL35 = mean(ResHIL35,1);




%%
figure()



yyaxis left
plot([avgsHIL33, avgsHIL34, avgsHIL35], '-s', 'LineWidth',1.5)
title("Performance of Hilbert relaxation")
xticks([1,2,3])
xticklabels({'(3,3)','(3,4)','(3,5)'})
yticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
yticklabels({'0','10','20','30','40','50','60','70','80','90','100'})
ylim([0,1])
xlim([0.95, 3.05])
xlabel("Experiment Size")
ylabel("Average Success Rate (%)")

yyaxis right
plot([avgtHIL33, avgtHIL34, avgtHIL35], '-o','LineWidth',1.5)
ylabel('Average Computation Time (s)')

grid on


%% PLots averages CNR

n=3;
m=3;
pathname = p{n,m};

SuccessCNR33 = [];
TimeCNR33 = [];
ResCNR33 = [];
DelCNR33 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_CNR(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessCNR33 = [SuccessCNR33; testCNR];
    TimeCNR33 = [TimeCNR33; tCNR];
    ResCNR33 = [ResCNR33; resCNR];
    DelCNR33 = [DelCNR33; delCNR];
end

avgtCNR33 = mean(TimeCNR33, 1);
avgsCNR33 = mean(SuccessCNR33, 1);
avgrCNR33 = mean(ResCNR33,1);
avgdCNR33 = mean(DelCNR33,1);

n=3;
m=4;
pathname = p{n,m};

SuccessCNR34 = [];
TimeCNR34 = [];
ResCNR34 = [];
DelCNR34 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_CNR(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessCNR34 = [SuccessCNR34; testCNR];
    TimeCNR34 = [TimeCNR34; tCNR];
    ResCNR34 = [ResCNR34; resCNR];
    DelCNR34 = [DelCNR34; delCNR];
end

avgtCNR34 = mean(TimeCNR34, 1);
avgsCNR34 = mean(SuccessCNR34, 1);
avgrCNR34 = mean(ResCNR34,1);
avgdCNR34 = mean(DelCNR34,1);


n=3;
m=5;
pathname = p{n,m};

SuccessCNR35 = [];
TimeCNR35 = [];
ResCNR35 = [];
DelCNR35 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_CNR(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessCNR35 = [SuccessCNR35; testCNR];
    TimeCNR35 = [TimeCNR35; tCNR];
    ResCNR35 = [ResCNR35; resCNR];
    DelCNR35 = [DelCNR35; delCNR];
end

avgtCNR35 = mean(TimeCNR35, 1);
avgsCNR35 = mean(SuccessCNR35, 1);
avgrCNR35 = mean(ResCNR35,1);
avgdCNR35 = mean(DelCNR35, 1);
%%
figure()

yyaxis left
plot([avgsCNR33, avgsCNR34, avgsCNR35], '-s', 'LineWidth',1.5)
title("Performance of CNR")
xticks([1,2,3])
xticklabels({'(3,3)','(3,4)','(3,5)'})
yticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
yticklabels({'0','10','20','30','40','50','60','70','80','90','100'})
ylim([0,1])
xlim([0.95, 3.05])
xlabel("Experiment Size")
ylabel("Average Success Rate (%)")

yyaxis right
plot([avgtCNR33, avgtCNR34, avgtCNR35], '-o','LineWidth',1.5)
ylabel('Average Computation Time (s)')

grid on


%% KKT


n=3;
m=3;
pathname = p{n,m};

SuccessKKT33 = [];
TimeKKT33 = [];
ResKKT33 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_KKTB(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessKKT33 = [SuccessKKT33; testKKTB];
    TimeKKT33 = [TimeKKT33; tKKTB];
    ResKKT33 = [ResKKT33; resKKTB];
end

avgtKKT33 = mean(TimeKKT33, 1);
avgsKKT33 = mean(SuccessKKT33, 1);
avgrKKT33 = mean(ResKKT33,1);


n=3;
m=4;
pathname = p{n,m};

SuccessKKT34 = [];
TimeKKT34 = [];
ResKKT34 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_KKTB(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessKKT34 = [SuccessKKT34; testKKTB];
    TimeKKT34 = [TimeKKT34; tKKTB];
    ResKKT34 = [ResKKT34; resKKTB];
end

avgtKKT34 = mean(TimeKKT34, 1);
avgsKKT34 = mean(SuccessKKT34, 1);
avgrKKT34 = mean(ResKKT34,1);



n=3;
m=5;
pathname = p{n,m};

SuccessKKT35 = [];
TimeKKT35 = [];
ResKKT35 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_KKTB(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessKKT35 = [SuccessKKT35; testKKTB];
    TimeKKT35 = [TimeKKT35; tKKTB];
    ResKKT35 = [ResKKT35; resKKTB];
end

avgtKKT35 = mean(TimeKKT35, 1);
avgsKKT35 = mean(SuccessKKT35, 1);
avgrKKT35 = mean(ResKKT35,1);

%%
figure()

yyaxis left
plot([avgsKKT33, avgsKKT34, avgsKKT35], '-s', 'LineWidth',1.5)
title("Performance of KKT relaxation")
xticks([1,2,3])
xticklabels({'(3,3)','(3,4)','(3,5)'})
yticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
yticklabels({'0','10','20','30','40','50','60','70','80','90','100'})
ylim([0,1])
xlim([0.95, 3.05])
xlabel("Experiment Size")
ylabel("Average Success Rate (%)")

yyaxis right
plot([avgtKKT33, avgtKKT34, avgtKKT35], '-o','LineWidth',1.5)
ylabel('Average Computation Time (s)')

grid on

%% JAC


n=3;
m=3;
pathname = p{n,m};

SuccessJAC33 = [];
TimeJAC33 = [];
ResJAC33 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_JAC(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessJAC33 = [SuccessJAC33; testJAC];
    TimeJAC33 = [TimeJAC33; tJAC];
    ResJAC33 = [ResJAC33; resJAC];
end

avgtJAC33 = mean(TimeJAC33, 1);
avgsJAC33 = mean(SuccessJAC33, 1);
avgrJAC33 = mean(ResJAC33,1);


n=3;
m=4;
pathname = p{n,m};

SuccessJAC34 = [];
TimeJAC34 = [];
ResJAC34 = [];
for k=1:50
    % loading HIL results
    filename = strcat('Results_JAC(',int2str(n),int2str(m),')',int2str(k),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    SuccessJAC34 = [SuccessJAC34; testJAC];
    TimeJAC34 = [TimeJAC34; tJAC];
    ResJAC34 = [ResJAC34; resJAC];
end

avgtJAC34 = mean(TimeJAC34, 1);
avgsJAC34 = mean(SuccessJAC34, 1);
avgrJAC34 = mean(ResJAC34,1);

%%
figure()

yyaxis left
plot([avgsJAC33, avgsJAC34], '-s', 'LineWidth',1.5)
title("Performance of Jacobian relaxation")
xticks([1,2])
xticklabels({'(3,3)','(3,4)'})
yticks([0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])
yticklabels({'0','10','20','30','40','50','60','70','80','90','100'})
ylim([0,1])
xlim([0.95, 2.05])
xlabel("Experiment Size")
ylabel("Average Success Rate (%)")

yyaxis right
plot([avgtJAC33, avgtJAC34], '-o','LineWidth',1.5)
ylabel('Average Computation Time (s)')

grid on

%%

% format long
% [avgrHIL33, avgrHIL34, avgrHIL35]
% [avgrCNR33, avgrCNR34, avgrCNR35]
% [avgrKKT33, avgrKKT34, avgrKKT35]
% [avgrJAC33, avgrJAC34]


% n=3;
% m=5;
% pathname = p{n,m};
% 
% SuccessJAC35 = [];
% TimeJAC35 = [];
% ResJAC35 = [];
% for k=1:50
%     % loading HIL results
%     filename = strcat('Results_JAC(',int2str(n),int2str(m),')',int2str(k),'.mat');
%     data = fullfile(pathname, filename);
%     load(data)
%     SuccessJAC35 = [SuccessJAC35; testJAC];
%     TimeJAC35 = [TimeJAC35; tJAC];
%     ResJAC35 = [ResJAC35; resJAC];
% end
% 
% avgtJAC35 = mean(TimeJAC35, 1);
% avgsJAC35 = mean(SuccessJAC35, 1);
% avgrJAC35 = mean(ResJAC35,1);


%{
CNR = 1;
HIL = 2;
KKTB = 3;
JAC = 4;



%Del = [];
TimeCNR = [];
SuccessCNR = [];
%Residual = [];
%Deg = [];

TimeHIL = [];
SuccessHIL = [];
TimeKKTB = [];
SuccessKKTB = [];
TimeJAC = [];
SuccessJAC = [];

Time = {};
Success = {};

disp('Initilization complete. Loading and storing Data.')

for n=3
    for m=n:5
        for k=1:50
            pathname = p{n,m};
            
            % loading CNR results
            filename = strcat('Results_CNR(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            %Del = [Del; DEL];
            SuccessCNR = [SuccessCNR; testCNR];
            TimeCNR = [TimeCNR; tCNR];
            %Residual = [Residual; Res];
            %Deg = [Deg; Degrees];
            
            % loading HIL results
            filename = strcat('Results_HIL(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            SuccessHIL = [SuccessHIL; testHIL];
            TimeHIL = [TimeHIL; tHIL];

            
            % loading KKTB results
            filename = strcat('Results_KKTB(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            SuccessKKTB = [SuccessKKTB; testKKTB];
            TimeKKTB = [TimeKKTB; tKKTB];
        end

        Time{n,m,CNR} = TimeCNR(:);
        Time{n,m,HIL} = TimeHIL(:);
        Time{n,m,KKTB} = TimeKKTB(:);
        Success{n,m,CNR} = SuccessCNR(:);
        Success{n,m,HIL} = SuccessHIL(:);
        Success{n,m,KKTB} = SuccessKKTB(:);
        
        
        %Del = [];
        TimeCNR = [];
        SuccessCNR = [];
        %Residual = [];
        %Deg = [];
        
        TimeHIL = [];
        SuccessHIL = [];
        TimeKKTB = [];
        SuccessKKTB = [];
    end
end

for n=3
    for m=n:4
        for k=1:50
            pathname = p{n,m};
            
            % loading JAC results
            filename = strcat('Results_JAC(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            SuccessJAC = [SuccessJAC; testJAC];
            TimeJAC = [TimeJAC; tJAC];
        end
        Time{n,m,JAC} = TimeJAC(:);
        Success{n,m,JAC} = SuccessJAC(:);
        
        TimeJAC = [];
        SuccessJAC = [];
    end
end

disp("Data Stored, computing averages")

avgT = {};
avgS = {};
for n=3
    for m=n:5
        avgT{n,m,CNR} = mean(Time{n,m,CNR},1);
        avgS{n,m,CNR} = mean(Success{n,m,CNR},1);
        
        avgT{n,m,HIL} = mean(Time{n,m,HIL},1);
        avgS{n,m,HIL} = mean(Success{n,m,HIL},1);
        
        avgT{n,m,KKTB} = mean(Time{n,m,KKTB},1);
        avgS{n,m,KKTB} = mean(Success{n,m,KKTB},1);
    end
end

for n=3
    for m=n:4
        avgT{n,m,JAC} = mean(Time{n,m,JAC},1);
        avgS{n,m,JAC} = mean(Success{n,m,JAC},1);
    end
end

disp("Plotting results")

for n=3
    for m=n:4
        figure();
        hold on
        plot((1:1:50), cumsum(Time{n,m,CNR}'), 'b-')
        plot((1:1:50), cumsum(Time{n,m,HIL}'), 'r-')
        plot((1:1:50), cumsum(Time{n,m,KKTB}'),'k-')
        plot((1:1:50), cumsum(Time{n,m,JAC}'), 'm-')
        xlabel('trial')
        ylabel('Cumulative computation time (s)')
        title(strcat('Problems of size (',int2str(n),',',int2str(m),')'))
        legendCell = {'CNR','HIL','KKTB','JAC'};
        legend(legendCell)
        
        figure();
        hold on
        plot((1:1:50), (1/50)*cumsum(Success{n,m,CNR}'), 'b-')
        plot((1:1:50), (1/50)*cumsum(Success{n,m,HIL}'), 'r-')
        plot((1:1:50), (1/50)*cumsum(Success{n,m,KKTB}'),'k-')
        plot((1:1:50), (1/50)*cumsum(Success{n,m,JAC}'), 'm-')
        xlabel('trial')
        ylabel('Cumulative successes rate (%)')
        ylim([0,1])
        yticks([0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
        yticklabels({0 10 20 30 40 50 60 70 80 90 100})
        title(strcat('Problems of size (',int2str(n),',',int2str(m),')'))
        legendCell = {'CNR','HIL','KKTB','JAC'};
        legend(legendCell)
    end
end

figure();
hold on
plot((1:1:50), cumsum(Time{3,5,CNR}'), 'b-')
plot((1:1:50), cumsum(Time{3,5,HIL}'), 'r-')
plot((1:1:50), cumsum(Time{3,5,KKTB}'),'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (3,5)')
legendCell = {'CNR','HIL','KKTB'};
legend(legendCell)

figure();
hold on
plot((1:1:50), (1/50)*cumsum(Success{3,5,CNR}'), 'b-')
plot((1:1:50), (1/50)*cumsum(Success{3,5,HIL}'), 'r-')
plot((1:1:50), (1/50)*cumsum(Success{3,5,KKTB}'),'k-')
xlabel('trial')
ylabel('Cumulative successes rate (%)')
ylim([0,1])
yticks([0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
yticklabels({0 10 20 30 40 50 60 70 80 90 100})
title('Problems of size (3,5)')
legendCell = {'CNR','HIL','KKTB'};
legend(legendCell)
%}