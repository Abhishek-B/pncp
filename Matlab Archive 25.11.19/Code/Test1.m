%{
I test here the success rate of the algorithm by default. I will generate
1000 random constructions from the algorithm, and see if numerically they
are SOS (or not).

In theory, this construction is guaranteed to not be SOS, but I test this
in practice and see if most of the constructed examples are not SOS for any
delta value.

I set the lower bound of delta to be on the order of -6; so if a value of
delta on this order or higher is found, we count that as SOS. While this is
quite strict, it removes any numerical doubts when something isn't SOS.

I'll also set a random seed so that these are reproducible results.

21/10/2019
Abhishek Bharwdaj
%}

%% tests

rng(1);
Nex = 2000;
count_ss = 0;
count_nss = 0;
n = 3;
m = 3;

for i=1:Nex
    
    disp(i)
    [X,Y,Z] = step1_1(n,m);
    V = step1_2(n,m,Z);
    v0 = step1_3(n,m,Z);
    W = step2_1(n,m,Z);
    v = step2_2(n,m, Z, W, v0, V);
    
    % SOS check
    SoS = isSOS(n,m,v,[v0 V],'mosek');
    if SoS==1
        count_ss = count_ss+1;
    elseif SoS==0
        count_nss = count_nss+1;
    end
end

%{
This doesn't take very long to run, so I wont bother saving the data, but
the results are (with 1000 trials) 429 SOS examples, and 571 non SOS examples. 
So I imagine we have somewhere between a 55-60% chance of making a non sos
form from this construction in practice.
%}

%% Plot

label = categorical({'SOS forms', 'Non-SOS forms'});
label = reordercats(label,{'SOS forms', 'Non-SOS forms'});
results = [count_ss, count_nss];
b = bar(label,results,0.4, 'FaceColor', 'flat');
b.CData(1,:) = [1 0 0];
title('KMSZ constructed forms')