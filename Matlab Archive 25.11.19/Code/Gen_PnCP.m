function [output] = Gen_PnCP(n, m, solver, method, params, attempts, rationalize)
% PCPgen - A Matlab function for generating Positive maps which are not
% Completely Positive (PnCP). 
%
% Abhishek Bhardwaj 2019.
%
% Inputs:
%           n,m - Integers; Determines the size of the spaces used.   
%        solver - String; Choice of solver to be used by Yalmip. 
%                 Accepted choices are all those in Yalmip, 
%                 'mosek', 'sedumi', etc.
%        method - String; Relaxation method for determining non-negativity.
%                 Currently supports {'CNR', 'HIL', 'KKTB', 'JAC'}.
%        params - Vector; Lists parameters for the relaxation method
%                 chosen. Each method has the following parameters;
%                 'CNR'  - [relax_degree] ([Integer])
%                 Others - [delta_est, relax_degree] ([float, Integer])
%      attempts - Integer; Maximum number of attempt to generate a PnCP map.
%   rationalize - 0: constructs a PnCP map with floating point arithmetic,
%                 1: constructs a PnCP map with rational arithmetic.
%
% Outputs: (cell containing PnCP map info)
%       
%       'CNR' - (1x4) cell with the following elements;
%                 delta - value of parameter which makes the map PnCP,
%                     v - non-SOS quadratic form,
%                     V - matrix of linear forms,
%               {x,y,z} - cell containing the random points used to
%               construct the form.
%      Others - (1x3) cell with the following elements;
%                     v - non-SOS quadratic form,
%                     V - matrix of linear forms,
%               {x,y,z} - cell containing the random points used to
%               construct the forms.
%
% See the article - "There are many more positive maps than completely
% positive maps" by Klep, McCullough, Sivic, Zalar - for detailed
% explanation of the representation of the linear and quadratic forms.
%


if rationalize==0
    
    if ~strcmp(method,'CNR') && ~strcmp(method,'HIL') && ~strcmp(method, 'KKTB') && ~strcmp(method,'JAC')
        error("Relaxation methods must be one of the following {'CNR','HIL','KKTB','JAC}")
    end
    
    if strcmp(method, 'CNR')
        if length(params)~=1
            error("CNR method takes 1 paramter; [relaxation_degree]")
        end
    elseif strcmp(method, 'HIL')
        if length(params)~=2
            error("HIL method takes 2 paramters; [delta_estimate, relaxation_degree]")
        end
    elseif strcmp(method, 'KKTB')
        if length(params)~=2
            error("KKTB method takes 2 paramters; [delta_estimate, relaxation_degree]")
        end
    elseif strcmp(method, 'JAC')
        if length(params)~=2
            error("JAC method takes 2 paramters; [delta_estimate, relaxation_degree]")
        end
    end        
                
    nng = 0;
    NNGtest_count = 0;
    
    while nng==0 && NNGtest_count<attempts
        %disp("Constructing non sos quadratic form")
        ss=1;
        construction_count = 0;
        while ss==1 && construction_count<100            
            % linear algebra step
            [x,y,z] = step1_1(n,m);
            V = step1_2(n,m,z);
            v0 = step1_3(n,m,z);
            W = step2_1(n,m,z);
            v = step2_2(n,m, z, W, v0, V);
            
            % checking if form is sos
            [ss, del] = isSOS(n,m,v,[v0 V],solver);
            construction_count = construction_count+1;
        end
        %disp("Testing Non negativity of constructed form")
        if strcmp(method, 'CNR')
            [nng, del, res] = step3(n,m,v,[v0 V],params(1), solver);
            NNGtest_count = NNGtest_count + 1;  
        elseif strcmp(method, 'HIL')
            [nng, res] = step3_HilbertB(n,m,v,[v0 V],params(1), params(2), solver);
            NNGtest_count = NNGtest_count + 1;
        elseif strcmp(method, 'KKTB')
            [nng, res] = step3_KKTB(n,m,v,[v0 V],params(1), params(2), solver);
            NNGtest_count = NNGtest_count + 1;
        elseif strcmp(method, 'JAC')
            [nng, res] = step3_jac(n,m,v,[v0 V],params(1), params(2), solver);
            NNGtest_count = NNGtest_count + 1;
        end
    end
    
    if nng==0
        fprintf("Construction not found.\nConsider increasing attempts limit, or changing method parameters")
        output = {};
    elseif nng==1
        disp("Construction complete")
        % Writing output in cell
        
        if strcmp(method, 'CNR')
            output = cell(1,4);
            output{1,1} = del;
            output{1,2} = v;
            output{1,3} = [v0 V];
            output{1,4} = {x,y,z};
        elseif strcmp(method, 'HIL')
            output{1,1} = v;
            output{1,2} = [v0 V];
            output{1,3} = {x,y,z};
        elseif strcmp(method, 'KKTB')
            output{1,1} = v;
            output{1,2} = [v0 V];
            output{1,3} = {x,y,z};
        elseif strcmp(method, 'JAC')
            output{1,1} = v;
            output{1,2} = [v0 V];
            output{1,3} = {x,y,z};
        end
    end
    


elseif rationalize==1
    
    if ~strcmp(method,'CNR') && ~strcmp(method,'HIL')
        error("Rational construction only supported with one of the following relaxation methods {'CNR','HIL'}")
    end
    
    if strcmp(method, 'CNR')
        if length(params)~=1
            error("CNR method takes 1 paramter; [relaxation_degree]")
        end
    elseif strcmp(method, 'HIL')
        if length(params)~=2
            error("HIL method takes 2 paramters; [delta_estimate, relaxation_degree]")
        end
    end
    
    
    
    nng = 0;

    NNGtest_count = 0;
    
    while nng==0 && NNGtest_count<attempts
        %disp("Constructing non sos quadratic form")
        ss=1;
        construction_count = 0;
        while ss==1 && construction_count<100
            
            % linear algebra step
            [x,y,z] = Rstep1_1(n,m);
            V = Rstep1_2(n,m,z);
            v0 = Rstep1_3(n,m,z);
            W = Rstep2_1(n,m,z);
            v = Rstep2_2(n,m, z, W, v0, V);
            
            % checking if form is sos
            [ss, del] = isSOS(n,m,v,[v0 V],solver);
            construction_count = construction_count+1;
        end
        %disp("Testing Non negativity of constructed form")
        if strcmp(method, 'CNR')
            [nng, del, res, G] = Rstep3(n,m,v,[v0 V],params(1), solver);
            NNGtest_count = NNGtest_count + 1;        
        elseif strcmp(method, 'HIL')
            [nng, G, res] = Rstep3HilbertB(n,m,v,[v0 V],params(1), params(2), solver);
            NNGtest_count = NNGtest_count + 1;
        end
    end
    
    if nng==0
        fprintf("Construction not found.\nConsider increasing attempts limit, or changing method parameters")
        output = {};
    elseif nng==1
        disp("Construction complete")
        
        % Writing output in cell
        
        if strcmp(method, 'CNR')
            output = {{del}, {v}, {[v0 V]}, {x,y,z} };
        elseif strcmp(method, 'HIL')
            output = { {v}, {[v0 V]}, {x,y,z} };
        end
    end
    



end






















end

