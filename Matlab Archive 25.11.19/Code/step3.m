function [ nng, del, res] = step3( n, m, v, V, l, solver)
% step3 - Computes a SOS decomposition of the final form in Algorithm 4.1.
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%          l - Integer which determines the SOS relaxation scale.
%     solver - Choice of solver to be used by Yalmip. 
%              Accepted choices are all those in Yalmip, 'mosek', 
%              'sedumi', etc.
%%%%%%%%%%%%%%%% REMOVED       time - Allowed time limit for solver to work on the generated 
%              SDP. 
%              NOTE: This only limits the solver time, not the total
%              time used by the method.
%
% Output:
%        nng - Binary {0,1} output determining Non-negativity of input.
%              0: Non-negativity can't be determined.
%              1: Non-negativity is numerically determined.
%        del - Floating point number which determines the non-negative,
%              non-SOS form over the Segre variety.
%        res - The residual between the SOS decomposition and the input.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Mosek. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.

%% SDP Argument variables.
X=sdpvar(m,1);
Y=sdpvar(n,1);
sdpvar delta;

%% Creating linear and quadratic forms from input.

f = v'*kron(kron(X,Y),kron(X,Y));

h_linforms = V'*kron(X,Y);
h_sos = h_linforms'*h_linforms;

%% SDP program and solve

% Yalmip SDP settings; changing max time, solver and verbose options.
ops = sdpsettings('solver',solver); %,'mosek.MSK_DPAR_OPTIMIZER_MAX_TIME',time);
ops.verbose = 0;

% Creating NNG/nonSOS form from alg 4.1 inputs.
poli = delta*f + 10*h_sos;

% Setting SOS relaxation; i.e. setting denominator.
relax = poli*(kron(X,Y)'*kron(X,Y))^l;

% Describing the sos program.
prog = sos(relax);

% Solving the optimization problem.
[sol, u, Q, res]=solvesos(prog, -delta, ops, delta);

% Checking the solve and the solution quality.
% The size(Q)~=0 condition is to combat a bug in Yalmip.
% This is fixable in Yalmip's sos module.
if size(Q)~=0 
    d1 = value(delta);
    if sol.problem==0 && floor(log10(d1))>-4 && floor(log10(res))<-6
        nng = 1;
        del = value(delta);
    else
        del = 0;
        nng = 0;
    end
else
    nng = 0;
    del = 0;
end

end