clc

trial = 10000;
success =  0;

for i=1:trial
    disp(i)
    n=3;
    m=3;
    l=1;
    maxdeg = 1;
    Z = step1_1(n,m);
    V = step1_2(n,m,Z);
    v0 = step1_3(n,m,Z);
    W = step2_1(n,m,Z);
    
    v = step2_2(n,m, Z, W, v0, V);
    
    del = step3(n,m,v,[v0 V], l);
    if del>0
        success = success+1;
    end
    save(sprintf('Example%i.mat',i), 'n', 'm', 'Z', 'V', 'v0', 'W', 'v', 'del')
end