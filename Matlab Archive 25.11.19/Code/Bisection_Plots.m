%{
Plots of the Bisection tests.
%}
set(0,'DefaultFigureWindowStyle','docked')
%% (3,3) Plots

Del = [];
Time = [];
Pass = [];
Residual = [];
Deg = [];

for i=1:50
    pathname = fileparts('C:\Users\Necrosis\Documents\PvsCP Latest\Experiments\(3,3)\');
    filename = strcat('Results_M(33)',int2str(i),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    Del = [Del; DEL];
    Pass = [Pass; Success];
    Time = [Time; timing];
    Residual = [Residual; Res];
    Deg = [Deg; Degrees];
end

avg33d = mode(Del);
avg33t = mean(Time, 1);
avg33s = mean(Pass, 1);
avg33r = mean(Residual,1);

figure();
hold on
plot((1:1:50), cumsum(Time(:,1)), 'b-')
plot((1:1:50), cumsum(Time(:,2)), 'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (3,3)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

figure();
hold on
plot((1:1:50), cumsum(Pass(:,1)), 'b-')
plot((1:1:50), cumsum(Pass(:,2)), 'k-')
xlabel('trial')
ylabel('Successes')
title('Problems of size (3,3)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

%% (3,4) Plots

Del = [];
Time = [];
Pass = [];
Residual = [];
Deg = [];

for i=1:50
    pathname = fileparts('C:\Users\Necrosis\Documents\PvsCP Latest\Experiments\(3,4)\');
    filename = strcat('Results_M(34)',int2str(i),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    Del = [Del; DEL];
    Pass = [Pass; Success];
    Time = [Time; timing];
    Residual = [Residual; Res];
    Deg = [Deg; Degrees];
end

avg34d = mode(Del);
avg34t = mean(Time, 1);
avg34s = mean(Pass, 1);
avg34r = mean(Residual,1);

figure();
hold on
plot((1:1:50), cumsum(Time(:,1)), 'b-')
plot((1:1:50), cumsum(Time(:,2)), 'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (3,4)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

figure();
hold on
plot((1:1:50), cumsum(Pass(:,1)), 'b-')
plot((1:1:50), cumsum(Pass(:,2)), 'k-')
xlabel('trial')
ylabel('Success Rate')
title('Problems of size (3,4)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

%% (3,5) Plots

Del = [];
Time = [];
Pass = [];
Residual = [];
Deg = [];

for i=1:50
    pathname = fileparts('C:\Users\Necrosis\Documents\PvsCP Latest\Experiments\(3,5)\');
    filename = strcat('Results_M(35)',int2str(i),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    Del = [Del; DEL];
    Pass = [Pass; Success];
    Time = [Time; timing];
    Residual = [Residual; Res];
    Deg = [Deg; Degrees];
end

avg35d = mode(Del);
avg35t = mean(Time, 1);
avg35s = mean(Pass, 1);
avg35r = mean(Residual,1);

figure();
hold on
plot((1:1:50), cumsum(Time(:,1)), 'b-')
plot((1:1:50), cumsum(Time(:,2)), 'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (3,5)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

figure();
hold on
plot((1:1:50), cumsum(Pass(:,1)), 'b-')
plot((1:1:50), cumsum(Pass(:,2)), 'k-')
xlabel('trial')
ylabel('Success Rate')
title('Problems of size (3,5)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

%% (4,4) Plots

Del = [];
Time = [];
Pass = [];
Residual = [];
Deg = [];

for i=1:50
    pathname = fileparts('C:\Users\Necrosis\Documents\PvsCP Latest\Experiments\(4,4)\');
    filename = strcat('Results_M(44)',int2str(i),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    Del = [Del; DEL];
    Pass = [Pass; Success];
    Time = [Time; timing];
    Residual = [Residual; Res];
    Deg = [Deg; Degrees];
end

avg44d = mode(Del);
avg44t = mean(Time, 1);
avg44s = mean(Pass, 1);
avg44r = mean(Residual,1);

figure();
hold on
plot((1:1:50), cumsum(Time(:,1)), 'b-')
plot((1:1:50), cumsum(Time(:,2)), 'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (4,4)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

figure();
hold on
plot((1:1:50), cumsum(Pass(:,1)), 'b-')
plot((1:1:50), cumsum(Pass(:,2)), 'k-')
xlabel('trial')
ylabel('Success Rate')
title('Problems of size (4,4)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

%% (4,5) Plots

Del = [];
Time = [];
Pass = [];
Residual = [];
Deg = [];

for i=1:50
    pathname = fileparts('C:\Users\Necrosis\Documents\PvsCP Latest\Experiments\(4,5)\');
    filename = strcat('Results_M(45)',int2str(i),'.mat');
    data = fullfile(pathname, filename);
    load(data)
    Del = [Del; DEL];
    Pass = [Pass; Success];
    Time = [Time; timing];
    Residual = [Residual; Res];
    Deg = [Deg; Degrees];
end

avg45d = mode(Del);
avg45t = mean(Time, 1);
avg45s = mean(Pass, 1);
avg45r = mean(Residual,1);

figure();
hold on
plot((1:1:50), cumsum(Time(:,1)), 'b-')
plot((1:1:50), cumsum(Time(:,2)), 'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (4,5)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)

figure();
hold on
plot((1:1:50), cumsum(Pass(:,1)), 'b-')
plot((1:1:50), cumsum(Pass(:,2)), 'k-')
xlabel('trial')
ylabel('Success Rate')
title('Problems of size (4,5)')
legendCell = {'CNR','Hilbert'}; 
legend(legendCell)
