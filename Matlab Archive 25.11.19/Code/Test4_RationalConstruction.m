%{




%}

%% Setting path variables.
p = cell(5,5);

cd ..
cd RData
cd '(3,3)'
p{3,3} = pwd;
cd ..
cd '(3,4)'
p{3,4} = pwd;
cd ..
cd '(3,5)'
p{3,5} = pwd;
cd ..
cd '(4,4)'
p{4,4} = pwd;
cd ..
cd '(4,5)'
p{4,5} = pwd;
cd ..
cd '(5,5)'
p{5,5} = pwd;
cd ..
cd ..
cd 'Code'

q = cell(5,5);

cd ..
cd FLData
cd '(3,3)'
q{3,3} = pwd;
cd ..
cd '(3,4)'
q{3,4} = pwd;
cd ..
cd '(3,5)'
q{3,5} = pwd;
cd ..
cd '(4,4)'
q{4,4} = pwd;
cd ..
cd '(4,5)'
q{4,5} = pwd;
cd ..
cd '(5,5)'
q{5,5} = pwd;
cd ..
cd ..
cd 'Code'


%% Generating Rational Data

% setting random seed to make this reproducible
rng(827)


time = cell(5,5);
for n=3:5
    for m=n:5
        pth = p{n,m};
        d = dir([pth, '/*.mat']);
        count = size(d,1);
        disp([n,m]);
        timing = [];
        
        tic;
        while count~=50
            %% Data
            [X, Y, Z] = Rstep1_1(n,m);
            V = Rstep1_2(n,m,Z);
            v0 = Rstep1_3(n,m,Z);
            W = Rstep2_1(n,m,Z);
            v = Rstep2_2(n,m, Z, W, v0, V);
            %% Check if form is SOS
            sos = isSOS(n,m,v,[v0 V],'mosek');
            if sos==0
                %disp([n,m,count+1])
                %% Save
                filename = strcat('Data(',int2str(n),int2str(m),')',int2str(count+1),'.mat');
                datafile = fullfile(pth, filename);
                save(datafile,'X','Y','Z','V','v0','v','W');
                count = count+1;
            end
        end
        tr = toc;
        
        
        pth = q{n,m};
        d = dir([pth, '/*.mat']);
        count = size(d,1);
        %disp([n,m]);
        timing = [];
        tic;
        while count~=50
            %% Data
            [X, Y, Z] = step1_1(n,m);
            V = step1_2(n,m,Z);
            v0 = step1_3(n,m,Z);
            W = step2_1(n,m,Z);
            v = step2_2(n,m, Z, W, v0, V);
            %% Check if form is SOS
            sos = isSOS(n,m,v,[v0 V],'mosek');
            if sos==0
                %disp([n,m,count+1])
                %% Save
                filename = strcat('Data(',int2str(n),int2str(m),')',int2str(count+1),'.mat');
                datafile = fullfile(pth, filename);
                save(datafile,'X','Y','Z','V','v0','v','W');
                count = count+1;
            end
        end
        tfl = toc;
 
        
        
        
        time{n,m} = [tr, tfl];
        disp([tr, tfl])
    end
end

%% Plots

Tr = (1/50)*[time{3,3}(1),time{3,4}(1),time{3,5}(1),time{4,4}(1),time{4,5}(1),time{5,5}(1)];
Tf = (1/50)*[time{3,3}(2),time{3,4}(2),time{3,5}(2),time{4,4}(2),time{4,5}(2),time{5,5}(2)];

figure()
plot(Tr, '-r', 'LineWidth',1)
grid on
xlabel("Problem size")
ylabel("Time (s)")
title("Average Construction Time")
xticks([1,2,3,4,5,6])
xticklabels({'(3,3)','(3,4)','(3,5)','(4,4)','(4,5)','(5,5)'})
xlim([0.95, 6.05])
hold on
plot(Tf, '-b', 'LineWidth',1)
legendcell = {'Rational', 'Floating Point'};
legend(legendcell)


