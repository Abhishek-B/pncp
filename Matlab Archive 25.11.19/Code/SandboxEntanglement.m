
%% creating entangled state similar to bell state

% state = randi(9,9);
% state = 0.5*(state+state');
% 
% while ~all(eig(state)>=0)
%     disp("state being generated")
%     state = state +eye(9);
% end
% disp("state generated")
    
state = zeros(9,9);

for i=1:3
    for j=1:3
        E = zeros(3,3);
        E(i,j) = 1;
        state = state + kron(E, E);
    end
end

state = (1/3)*state;

[nrow, ncol] = size(state);

if nrow~=ncol
    disp("error, state matrix should be square")
end
if ~issymmetric(state)
    disp("error, state matrix needs to be symmetric")
end

%% building function Fdelta
n = 3;
m = 3;



nng = 0;
count = 0;
% creating form
while nng==0 && count<100
    ss = 1;
    disp("building function")
    while ss==1
        [X,Y,Z] = Rstep1_1(n,m);
        V = Rstep1_2(n,m,Z);
        v0 = Rstep1_3(n,m,Z);
        W = Rstep2_1(n,m,Z);
        v = Rstep2_2(n,m, Z, W, v0, V);
        [ss, del] = isSOS(n,m,v,[v0 V],'mosek');
    end
    disp("testing nng")
    % testing NNG
    [nng, G, res] = Rstep3HilbertB(n,m,v,[v0 V],2,1,'mosek');
    count = count+1;
    disp(count)
end
disp("finished")


%% SDP vars to extract Phi from Fdelta

X = sdpvar(n,1);
Y = sdpvar(m,1);
Z = kron(X,Y);
f = double(v')*kron(Z,Z);                                             % defining the non-sos quad form f
h = double(V')*Z;                                                     % The linear forms h constructed in step 1

F = f + (h'*h); 

[C, V1] = coefficients(F, X);


%% defining operator Phi

Phi = cell(n,n);

symmat = X*X';
for i=1:n
    for j=i:n
        
        for k=1:length(V1)
            
            if class(V1(k)-symmat(i,j))=='double'
            
                poly = C(k);
                Q = sdpvar(m,m,'symmetric');
                SOL = [coefficients(poly-Y'*Q*Y, Y) == 0];
                optimize(SOL,[], sdpsettings('verbose',0));
                G = value(Q);
                if j==i
                    Phi{i,j} = G;
                elseif j~=i
                    Phi{i,j} = 0.5*G;
                end
            end
        end
    end
end

for i=2:n
    for j=1:i-1
        Phi{i,j} = Phi{j,i};
    end
end


%% Checking Phi to be nonnegative on State
               
bstate = mat2cell(state, [3,3,3],[3,3,3]);

M = cell(3,3);
for i=1:n
    for j=1:n
        L = bstate{i,j};
        Lmap = zeros(3,3);
        for k=1:3
            for l=1:3
                    Lmap = Lmap + L(k,l)*Phi{k,l};
            end
        end
        M{i,j} = Lmap;
    end
end


% for i=1:n
%     for j=1:i
%         if j<i
%             M{i,j} = M{j,i}';
%         end
%     end
% end
        

M = cell2mat(M);

if any(eig(M)<0)
    disp("non-negative map on state found")
end

%{
A Successful example showing negativity of the state was found to be 
39.75*X(1)^2*Y(1)^2-77*X(1)^2*Y(1)*Y(2)-4*X(1)^2*Y(1)*Y(3)-15*X(1)*X(2)*Y(1)^2+80*X(1)*X(2)*Y(1)*Y(2)+1.5*X(1)*X(2)*Y(1)*Y(3)+45*X(1)*X(3)*Y(1)^2-19*X(1)*X(3)*Y(1)*Y(2)-19.5*X(1)*X(3)*Y(1)*Y(3)+12*X(1)*X(4)*Y(1)^2-12*X(1)*X(4)*Y(1)*Y(2)-3*X(1)*X(4)*Y(1)*Y(3)+40*X(1)^2*Y(2)^2+X(1)^2*Y(2)*Y(3)-58*X(1)*X(2)*Y(2)^2-9*X(1)*X(2)*Y(2)*Y(3)-16*X(1)*X(3)*Y(2)^2+11*X(1)*X(3)*Y(2)*Y(3)+6*X(1)*X(4)*Y(2)^2-7*X(1)*X(4)*Y(2)*Y(3)+1.75*X(1)^2*Y(3)^2+3.5*X(1)*X(2)*Y(3)^2-1.5*X(1)*X(3)*Y(3)^2-2*X(1)*X(4)*Y(3)^2+14.75*X(2)^2*Y(1)^2-22*X(2)^2*Y(1)*Y(2)+5.5*X(2)^2*Y(1)*Y(3)-7*X(2)*X(3)*Y(1)^2+33*X(2)*X(3)*Y(1)*Y(2)+2.5*X(2)*X(3)*Y(1)*Y(3)-16*X(2)*X(4)*Y(1)^2+7*X(2)*X(4)*Y(1)*Y(2)+4*X(2)*X(4)*Y(1)*Y(3)+28*X(2)^2*Y(2)^2-10*X(2)^2*Y(2)*Y(3)+4*X(2)*X(3)*Y(2)^2+2*X(2)*X(3)*Y(2)*Y(3)+2*X(2)*X(4)*Y(2)^2-X(2)*X(4)*Y(2)*Y(3)+1.75*X(2)^2*Y(3)^2-1.5*X(2)*X(3)*Y(3)^2-2*X(2)*X(4)*Y(3)^2+3*X(3)^2*Y(1)^2+4*X(3)^2*Y(1)*Y(3)-4*X(3)*X(4)*Y(1)*Y(3)+5*X(3)^2*Y(2)^2-2*X(3)*X(4)*Y(2)^2-X(3)*X(4)*Y(2)*Y(3)+4.5*X(3)^2*Y(3)^2+7*X(3)*X(4)*Y(3)^2+5*X(4)^2*Y(1)^2-2*X(4)^2*Y(1)*Y(3)+3*X(4)^2*Y(2)^2-4*X(4)^2*Y(2)*Y(3)+4*X(4)^2*Y(3)^2

Potentially this will be reproducible, but if not, it is written here for
the paper's example.

%}







