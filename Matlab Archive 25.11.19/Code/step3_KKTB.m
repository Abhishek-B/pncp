function [ nng, res ] = step3_KKTB(n,m,v,V,d,delta,solver)
% step3_KKT - Computes a SOS decomposition of the final form in Algorithm 4.1
%             using the KKT variety and bisecting over delta.
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%          d - Integer upper bound on the degree of the decision
%              polynomials 'phi' and 'eta'.
%      delta - Estimate of delta to check non-negativity with.
%     solver - Choice of solver to be used by Yalmip. 
%              Accepted choices are all those in Yalmip, 'mosek', 
%              'sedumi', etc.
%
% Output:
%        nng - Binary {0,1} output determining Non-negativity of input.
%              0: Non-negativity can't be determined.
%              1: Non-negativity is numerically determined.
%        res - The residual between the SOS decomposition and the input.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Mosek. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.
%
% In practice this method should not be used with large degrees 'd'. If low
% degrees don't give a satisfactory solution, the problem should either be
% reformulated, or a different method used. 




%% sdpvar's

% Function Argument variables
X = sdpvar(n,1);
Y = sdpvar(m,1);
Z = kron(X,Y);
sdpvar lambda        

% Monomial list in variables
mon = monolist([X;Y],d);

% Gram 'matrices' for the relaxations
coeffgrad = sdpvar(n+m, size(mon,1),'full');  
coeffcons = sdpvar(1, size(mon,1)); 

% Polynomial representation of the relaxations.
etavec = coeffgrad*mon;                                 
phivec = coeffcons*mon;   

%% Decision variables from the relaxation
decvar = [coeffgrad(:); coeffcons(:)];

%% linear and quadratic forms

% Linear and quadratic form from the inputs V and v
f = v'*(kron(Z,Z));   
h = V'*Z;            

% Non-SOS form generated via Alg 4.1 to be tested for non-negativity.
F = delta*f + (h'*h);      

%% Variety constraints

% Defining the variety of the sphere
constraint = X'*X + Y'*Y - 1; 

%% KKT constraints

% Columns are the gradients of the variety constraints
grads = jacobian(constraint, [X;Y])';                         

% Gradients of the function F
gradF = jacobian(F, [X;Y])';    

% Sum of the gradients of F and the variety
gradients = gradF + grads*lambda;      

%% sos program

% KKT relaxation for optimality.
poly = F - etavec'*gradients - phivec'*constraint;

% SOS program to solve
prog = sos(poly);

%% sdp solver

% SDP setting through YALMIP --- This needs to be updated to be an input
% for the user.
ops = sdpsettings('solver', solver);
ops.verbose = 0;

% Solving the optimization problem.
[sol, u, Q, res]=solvesos(prog, [], ops, decvar);          

% Checking the solve and the solution quality.
% The size(Q)~=0 condition is to combat a bug in Yalmip.
% This is fixable in Yalmip's sos module.
if size(Q)~=0    
    if sol.problem==0 && floor(log10(res))<-6
        nng = 1;
    else
        nng = 0;
    end
else
    nng = 0;
end


end
