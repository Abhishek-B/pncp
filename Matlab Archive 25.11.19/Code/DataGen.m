%{




%}

%% Setting path variables.
p = cell(5,5);

cd ..
cd EXPTS
cd '(3,3)'
p{3,3} = pwd;
cd ..
cd '(3,4)'
p{3,4} = pwd;
cd ..
cd '(3,5)'
p{3,5} = pwd;
cd ..
cd '(4,4)'
p{4,4} = pwd;
cd ..
cd '(4,5)'
p{4,5} = pwd;
cd ..
cd '(5,5)'
p{5,5} = pwd;
cd ..
cd ..
cd 'Code'

%% Generating Data
for n=3:5
    for m=n:5
        pth = p{n,m};
        d = dir([pth, '/*.mat']);
        count = size(d,1);
        disp([n,m]);
        while count~=50
            %% Data
            [X, Y, Z] = step1_1(n,m);
            V = step1_2(n,m,Z);
            v0 = step1_3(n,m,Z);
            W = step2_1(n,m,Z);
            v = step2_2(n,m, Z, W, v0, V);
            %% Check if form is SOS
            sos = isSOS(n,m,v,[v0 V],'mosek');
            if sos==0
                disp([n,m,count+1])
                %% Save
                filename = strcat('Data(',int2str(n),int2str(m),')',int2str(count+1),'.mat');
                datafile = fullfile(pth, filename);
                save(datafile,'X','Y','Z','V','v0','v','W');
                count = count+1;
            end
        end
    end
end