
%% creating entangled state similar to bell state

A=3;
B=5;



state = randi(A*B, A*B);
state = 0.5*(state+state');

while ~all(eig(state)>=0)
    %disp("state being generated")
    state = state +eye(A*B);
end
disp("state generated")
% state = zeros(9,9);
% 
% for i=1:3
%     for j=1:3
%         E = zeros(3,3);
%         E(i,j) = 1;
%         state = state + kron(E, E);
%     end
% end
state2 = (1/trace(state))*state;

[nrow, ncol] = size(state);

if nrow~=ncol
    disp("error, state matrix should be square")
end
if ~issymmetric(state)
    disp("error, state matrix needs to be symmetric")
end

%% Using PCPentchk

tic;
output = Ent_PnCP(A,B,3,state, 10);
if length(output)>1
    Phi = output{1,1};
    M = output{1,2};
end
tend = toc;







