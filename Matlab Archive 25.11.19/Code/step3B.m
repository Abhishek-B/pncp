function [ success ] = step3B( n,m,v,V,l,delta )
% step3B - Computes a SOS decomposition of the final form in Algorithm 4.1.
%          with delta beign an input rather than a parameter.
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%          l - Integer which determines the SOS relaxation scale.
%        del - Real estimate of delta
%
% Output:
%        Displays wether SOS decomposition is possible with the input del.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Mosek. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.

%% SDP variables.
X=sdpvar(n,1);
Y=sdpvar(m,1);

%% Creating the form 'f'.
f = v'*kron(kron(X,Y),kron(X,Y));

%% Creating the linear forms h_{0},...,h_{d}

h_linforms = V'*kron(X,Y);
h_sos = h_linforms'*h_linforms;


%{ 
Old solver method.
%% Solving the minimization problem.

sdpsettings('solver','sedumi');              % Selecting solver. 
                                             % The user can change this based on personal preference.

poli = delta*f + (2^8)*h_sos;                % Describing quadratic form

prog = sos(poli*(kron(X,Y)'*kron(X,Y))^l);   % Describing programm

solvesos(prog, -delta, [], delta);           % Running optimizer

del = value(delta);                          % Required solution
%}

%% Solving the minimization problem.

ops = sdpsettings('solver','mosek');              % Selecting solver. 
ops.verbose = 0;                                  % The user can change this based on personal preference.

poli = delta*f + 10*h_sos;                        % Describing quadratic form
relax = poli*(kron(X,Y)'*kron(X,Y))^l;

prog = sos(relax);   % Describing programm

[sol, u, Q]=solvesos(prog, [], ops, []);   % Running optimizer
if sol.problem==0
    disp('SOS decomposition is possible.')
    success = 1;
else 
    disp('SOS decomposition not found.')
    success = 0;
end

end

