function [ nng, G, res] = Rstep3HilbertB(n,m,v,V,d,delta,solver)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


%% sdpvar's
X = sdpvar(n,1);
Y = sdpvar(m,1);
Z = kron(X,Y);
monlist = monolist([X;Y], d); 
monlist = monlist(2:end);                                       % We do not consider the monomial '1'.
G = sdpvar(size(monlist,1), size(monlist,1),'symmetric');       % matrix of coefficients
phi = monlist'*G*monlist;                                       % vector of decision functions phi

%% linear and quadratic forms
f = double(v')*kron(Z,Z);                                             % defining the non-sos quad form f
h = double(V')*Z;                                                     % The linear forms h constructed in step 1

F = delta*f + (h'*h);                                                 % non-negative but not sos form

%% sos program
prog = [sos(phi*F);trace(G)==1];

%% decision variables
coeff = reshape(G, [1, size(monlist,1)*size(monlist,1)]);      % reshaping coefficient into vector - this should be same as G(:), test this on matlab.


%% sdp solver
ops = sdpsettings('solver', solver);
ops.verbose = 0;


[sol, u, Q]=solvesos(prog, [], ops, coeff);                    % Running optimizer

if size(Q)~=0    
    G1 = reshape(value(coeff),[size(monlist,1),size(monlist,1)]);
    res = max(coefficients((monlist'*G1*monlist)*F - u{1,1}'*Q{1,1}*u{1,1}));
    
    if sol.problem==0 && res<=1e-6
        %disp('SOS decomposition is possible.')
        G = G1;
        nng = 1;
    else
        %disp('SOS decomposition not found.')
        nng = 0;
        G = zeros(size(monlist,1), size(monlist,1));
    end
else
    nng = 0;
    G = zeros(size(monlist,1), size(monlist,1));
    res = 0;
end


%% Testing parrilo rationalization

%sdisplay(u{1,1})


























end

