function [nng,res,G] = step3_denominator(n,m,v,V,d)
% Computes an SOS decomposition using Artin's theorem for the final form 
% generated in Alg 4.1.
%
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%     solver - Choice of solver to be used by Yalmip. 
%              Accepted choices are all those in Yalmip, 'mosek', 
%              'sedumi', etc.
%              NOTE: This is a non-linear SDP, and will need PENLAB, or
%              some other non-linear SDP solver.
%
% Output:
%        nng - Binary {0,1} output determining Non-negativity of input.
%              0: Non-negativity can't be determined.
%              1: Non-negativity is numerically determined.
%        res - The residual between the SOS decomposition and the input.
%          G - Gram matrix of the denominator.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Mosek. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.
%% sdpvar's

% Function argument variables
X = sdpvar(n,1);
Y = sdpvar(m,1);
Z = kron(X,Y);
sdpvar delta;

% List of monomials in variables.
% Note that we remove the constant 1.
monlist = monolist([X;Y], d); 
monlist = monlist(2:end);

% Gram matrix for denominator
G = sdpvar(size(monlist,1), size(monlist,1),'symmetric');      

% Polynomial representation of denominator.
phi = monlist'*G*monlist;                                       

%% linear and quadratic forms

% Generating linear and quadratic forms from the inputs v and V.
f = v'*kron(Z,Z);                                             
h = V'*Z;                                                      

% Non-SOS form generated through Alg 4.1.
F = delta*f + (h'*h);  

%% Writing the sos program
prog = sos(phi*F);

%% Reshaping decision vars into vector
coeff2 = G(:);

%% sdp solver

% Setting SDP options  --- This needs to be updated into an input for the
% user.
ops = sdpsettings('solver', 'penlab');
ops.verbose = 0;

% Solving the optimization problem. The objective is to maximize the
% variable delta.
[sol, u, Q, res]=solvesos(prog, -delta, ops, [delta coeff2]);               

% Checking the solve and solution quality.
% There should be a check on the size of Q to combat a Yalmip bug, but
% since this function is impractical I will leave it for now.
if sol.problem==0
    G = reshape(value(coeff2),[size(monlist,1),size(monlist,1)]);
    nng = 1;
else
    nng = 0;
end

end

