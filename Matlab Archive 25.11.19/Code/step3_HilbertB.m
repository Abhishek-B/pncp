function [nng, res] = step3_HilbertB(n,m,v,V,d,delta,solver)
% Computes an SOS decomposition using Artin's theorem for the final form 
% generated in Alg 4.1.
%
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%      delta - Real valued estimate to check for non-negativity.
%     solver - Choice of solver to be used by Yalmip. 
%              Accepted choices are all those in Yalmip, 'mosek', 
%              'sedumi', etc.
%%%%%%%%%%%%%%%%% REMOVED       time - Allowed time limit for solver to work on the generated 
%              SDP. 
%              NOTE: This only limits the solver time, not the total
%              time used by the method.
%
% Output:
%        nng - Binary {0,1} output determining Non-negativity of input.
%              0: Non-negativity can't be determined.
%              1: Non-negativity is numerically determined.
%        res - The residual between the SOS decomposition and the input.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Mosek. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.

%% sdpvar's

% Argument variables
X = sdpvar(n,1);
Y = sdpvar(m,1);
Z = kron(X,Y);

% Monomials up to degree d.
% We remove the constant 1.
mon = monolist([X;Y], d); 
mon = mon(2:end);

% Symbolic Gram matrix for denominator.
G = sdpvar(size(mon,1), size(mon,1),'symmetric'); 

% Polynomial representation of denominator. 
phi = mon'*G*mon;                                       

%% linear and quadratic forms

% Generating linear and quadratic forms from input.
f = v'*kron(Z,Z);                                            
h = V'*Z;                                                    

% NNG but nonSOS form from alg 4.1.
F = delta*f + (10)*(h'*h);  

%% sos program
% Setting trace constraint to avoid trivial solution.
prog = [sos(phi*F);trace(G)==1];

%% decision variables
% Reshaping gram matrix variables in vector.
coeff = G(:);   

%% sdp solver

% Yalmip SDP settings; changing max time, solver and verbose options.
ops = sdpsettings('solver', solver); %,'mosek.MSK_DPAR_OPTIMIZER_MAX_TIME',time);
ops.verbose = 0;

% Solving the optimization problem.
[sol, u, Q, res]=solvesos(prog, [], ops, coeff);

% Checking the solve and the solution quality.
% The size(Q)~=0 condition is to combat a bug in Yalmip.
% This is fixable in Yalmip's sos module.
if size(Q)~=0    
    if sol.problem==0 && floor(log10(res))<-6
        nng = 1;
    else
        nng = 0;
    end
else
    nng = 0;
end

end

