
n = 3;
m = 3;
d = 1;
delta = 1;
solver = 'mosek';
% 
load('RationalizationExample.mat')
% nng = 0;
% count=0;
% 
% while nng==0 && count<20
%     iter = 0;
%     ss = 1;
%     while ss==1 && iter <20
%         [x,y,z] = Rstep1_1(n,m);
%         V = Rstep1_2(n,m,z);
%         v0 = Rstep1_3(n,m,z);
%         W = Rstep2_1(n,m,z);
%         v = Rstep2_2(n,m, z, W, v0, V);
%         
%         % checking sos state;
%         ss = isSOS(n,m,v,[v0 V],'mosek');
%         iter = iter+1;
%     end
%     disp("NSOS form constructed")
% 
%     [nng, G, res] = Rstep3HilbertB(n,m,v,[v0 V],d,delta,solver);
%     count=count+1;
%     disp('count is')
%     disp(count)
% end
% 
% if nng==1
%     disp('success')
% end
%% sdpvar's
X = sdpvar(n,1);
Y = sdpvar(m,1);
Z = kron(X,Y);
monlist = monolist([X;Y], d); 
monlist = monlist(2:end);                                       % We do not consider the monomial '1'.
g = sdpvar(size(monlist,1), size(monlist,1),'symmetric');       % matrix of coefficients
phi = monlist'*g*monlist;

%% linear and quadratic forms
f = double(v')*kron(Z,Z);                                             % defining the non-sos quad form f
h = double(V')*Z;                                                     % The linear forms h constructed in step 1

F = delta*f + (h'*h);                                                 % non-negative but not sos form
%% sos program
prog = [sos(phi*F);trace(g)==1];

%% decision variables
coeff = g(:);      % reshaping coefficient into vector - this should be same as G(:), test this on matlab.


%% sdp solver
ops = sdpsettings('solver', solver);
ops.verbose = 1;


[sol, u, Q,res]=solvesos(prog, [], ops, coeff);                    % Running optimizer

%% numerical SOS is true, trying rational sos 

W = u{1,1};
S = Q{1,1};
G = reshape( value( coeff) , [6,6]);

syms a b c












