function [ del ] = solver(n,m,solver,l,k,d,delta)
% testfun - Used to test the different solvers for the PvCP problem.
%
% Inputs:
%                 n,m    - Integers which determine the size of the spaces
%                          used.
%
%                 solver - Determines which solver is used for step 3 of algorithm
%                          4.1. The options for the 'solver' input are the following;
%                          'standard' - uses step3 function;
%                          'grad'     - uses step3_gradient function;
%                          'direc'    - uses step3_directional function;
%                          'KKT'      - uses step3_KKT function;
%                          'lingrad'  - uses step3step3_lingrad;
%                          'linKKT'   - uses step3_linKKT.
%
%                      l - Integer. Used with the 'standard' solver option. 
%
%                      k - Integer. Used with the 'direc' solver option.
%
%                      d - Integer. Used with the 'grad', 'direc' and 'KKT' solver
%                          options.
%
%                          See the corresponding function files for details
%                          on the inputs l,k,d.
%
% The inputs should be entered as a cell '{n,m,solver,l,k,d}'.
%
% Output:
%        del - Floating point number which determines the non-negative,
%              non-SOS form over the Segre variety.

%%
clc

Z = step1_1(n,m);
Vi = step1_2(n,m,Z);
v0 = step1_3(Z);
W = step2_1(n,m,Z);
v = step2_2(n,m, Z, W, v0, Vi);
V = [v0 Vi];

del=0;

if strcmp(solver,'standard')
    del = step3(n,m,v,V,l);
elseif strcmp(solver,'grad')
    del = step3_grad(n,m,v,V,d);
elseif strcmp(solver,'direc')
    del = step3_directional(n,m,v,V,d,k);
elseif strcmp(solver,'KKT')
    del = step3_KKT(n,m,v,V,d);
elseif strcmp(solver, 'lingrad')
    del = step3_lingrad(n,m,v,V,d,delta);
elseif strcmp(solver, 'linKKT')
    del = step3step3_linKKT(n,m,v,V,d,delta)
else
    msg='solver not chosen';
    error(msg);
end

disp('computed delta is')
disp(del)

end
