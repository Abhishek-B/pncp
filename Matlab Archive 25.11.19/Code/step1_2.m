function [ V ] = step1_2(n,m,Z)
% Step1_2 - Generates the linear forms h1,...,hd for the algorithm 4.1
%           See the function file for more details. 
% Inputs:
%        n,m  - integers which determine the size of the spaces
%               used.
%           Z - A matrix containging random points on the Segre
%               variety as column vectors. 
%               The size of Z is needed to be (n*m, (n-1)*(m-1)), 
%               i.e. Z has n*m rows and (n-1)*(m-1) columns. 
% 
% Outputs:
%         V - A matrix containing random vectors from the kernel of Z' as
%             columns. The columns define the linear forms h_{1},...,h_{d} 
%             (d=n+m-2) for the algorithm 4.1. 


%{
If the size of the input matrix Z is incorrect the function stops. 
We use the built in Matlab function 'null' to find the required kernels. 

The random vectors are chosen using random linear combinations sampled from
the Gaussian distribution. This can (and should) be changed to the required
sampling density. 

%}

%% Initialising parameters.

e = (n-1)*(m-1);
d = n+m-2;

%% Checking the dimensions of input Z.

if ~isequal(size(Z),[n*m,e+1])
    msg = 'The dimensions of Z are not [nm,(n-1)(m-1)+1].';
    error(msg);
end

%% Computing the kernel of Z'. 

Kernel = null(Z');

%% Finding d random vectors in the kernel and storing as V.

temp = randi([-floor((n+m)/2),floor((n+m)/2)],size(Kernel,2), d);
V = Kernel*temp;

end

