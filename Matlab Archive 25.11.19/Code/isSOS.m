function [ ss ,del ] = isSOS(n,m, v, V, solver)

%{
Function which checks if a given form is SOS or not.
%}

%% Variable setup
x = sdpvar(n,1);
y = sdpvar(m,1);
z = kron(x,y);
sdpvar delta

%% Defining functions
f = double(v')*kron(z,z);	%										defining the non-sos quad form f
h = double(V')*z;           %										The linear forms h constructed in step 1

F = delta*f + (h'*h);  

%% SOS program
prog = [sos(F)];

ops = sdpsettings('solver', solver);
ops.verbose = 0;


[sol, u, Q, res]=solvesos(prog, -delta, ops, delta);

del = value(delta);
if sol.problem==0 && floor(log10(del))>-6 && floor(log10(res))<-6
   	ss = 1; % 	True
else
   	ss = 0; % 	False
end


end