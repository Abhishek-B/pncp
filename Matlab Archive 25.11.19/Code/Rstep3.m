function [success, del, res, G] = Rstep3( n, m, v, V, l, solver )
% step3 - Computes a SOS decomposition of the final form in Algorithm 4.1.
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%          l - Integer which determines the SOS relaxation scale.
%
% Output:
%        del - Floating point number which determines the non-negative,
%              non-SOS form over the Segre variety.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Sedumi. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.

%% SDP variables.
X=sdpvar(m,1);
Y=sdpvar(n,1);
sdpvar delta;

%% Creating the form 'f'.
f = double(v')*kron(kron(X,Y),kron(X,Y));

%% Creating the linear forms h_{0},...,h_{d}

h_linforms = double(V')*kron(X,Y);
h_sos = h_linforms'*h_linforms;


%% Solving the minimization problem.

ops = sdpsettings('solver',solver);              
ops.verbose = 0;                                            

poli = delta*f + h_sos;                
relax = poli*(kron(X,Y)'*kron(X,Y))^l;

prog = sos(relax);   

[sol, u, Q]=solvesos(prog, -delta, ops, delta);           

if size(Q)~=0 
    d1 = value(delta);
    res = max(coefficients((d1*f + h_sos)*(kron(X,Y)'*kron(X,Y))^l - u{1,1}'*Q{1,1}*u{1,1}));
    if sol.problem==0 && d1>1 && res<=1e-6
        %disp('SOS decomposition is possible.')
        success = 1;
        del = value(delta);
        G = Q{1,1};
    else
        %disp('SOS decomposition not found.')
        del = 0;
        success = 0;
        G = zeros(n*m,n*m);
    end
else
    success = 0;
    del = 0;
    res = 0;
    G = zeros(n*m,n*m);
end
%}


end