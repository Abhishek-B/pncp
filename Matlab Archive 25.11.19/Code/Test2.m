%{
Here I want to see how high we can actually go with (n,m).
Since the CNR is the fastest, I will use that, and then see if I
want to test with others.

Probably also good to set a random seed to make these results reproducible
as well.

I want to generate a random (n,m) form and then see if it is NNG and nSOS,
and I want to see how high we can go with (n,m). So I'll set the degree
limit on the CNR to be 2.

--update: after a couple of test, degree 2 SDP takes significantly longer
than just making a new example. So I'll limit the degree to 1 and instead
set a limit of 20 examples to be tested.

21/10/2019
Abhishek Bhardwaj
%}

rng(1);
time = {};
success = {};

for n=3:5
    for m=n:5
        
        ss = 1; % Initialize SOS to be true
        NNG = 0; % initialize non-negativity to be false
        count_ex = 0;
        
        disp([n,m])
        
        % until we get a successful example we repeat
        tic;
        while NNG==0 && count_ex<21
            % first we construct a nSOS form
            while ss==1
                [X,Y,Z] = step1_1(n,m);
                V = step1_2(n,m,Z);
                v0 = step1_3(n,m,Z);
                W = step2_1(n,m,Z);
                v = step2_2(n,m, Z, W, v0, V);
                
                % SOS check
                ss = isSOS(n,m,v,[v0 V],'mosek');
            end
            % now we have a nSOS form constructed, so we test NNG
            [NNG, del, res] = step3(n,m,v,[v0 V],1,'mosek');
%             if NNG==0
%                 disp("trying degree 2")
%                 [NNG, del, res] = step3(n,m,v,[v0 V],2,'mosek'); % if l=1 doesn't work we try l=2
%             end
            count_ex = count_ex+1;
            disp(count_ex)
        end
        time{n,m} = toc;
        if NNG==1
            success{n,m}=1;
        elseif NNG==0
            success{n,m}=0;
        end
    end
end

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        