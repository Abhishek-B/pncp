%{
Plots of the results of the algorithms tested
%}
set(0,'DefaultFigureWindowStyle','docked')

% setting path variables.
p = cell(5,5);

cd ..
cd EXPTS
cd '(3,3)'
p{3,3} = pwd;
cd ..
cd '(3,4)'
p{3,4} = pwd;
cd ..
cd '(3,5)'
p{3,5} = pwd;
cd ..
cd '(4,4)'
p{4,4} = pwd;
cd ..
cd '(4,5)'
p{4,5} = pwd;
cd ..
cd ..
cd 'Code'
%% Timing Plots

CNR = 1;
HIL = 2;
KKTB = 3;
JAC = 4;



%Del = [];
TimeCNR = [];
SuccessCNR = [];
%Residual = [];
%Deg = [];

TimeHIL = [];
SuccessHIL = [];
TimeKKTB = [];
SuccessKKTB = [];
TimeJAC = [];
SuccessJAC = [];

Time = {};
Success = {};

disp('Initilization complete. Loading and storing Data.')

for n=3
    for m=n:5
        for k=1:50
            pathname = p{n,m};
            
            % loading CNR results
            filename = strcat('Results_CNR(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            %Del = [Del; DEL];
            SuccessCNR = [SuccessCNR; testCNR];
            TimeCNR = [TimeCNR; tCNR];
            %Residual = [Residual; Res];
            %Deg = [Deg; Degrees];
            
            % loading HIL results
            filename = strcat('Results_HIL(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            SuccessHIL = [SuccessHIL; testHIL];
            TimeHIL = [TimeHIL; tHIL];

            
            % loading KKTB results
            filename = strcat('Results_KKTB(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            SuccessKKTB = [SuccessKKTB; testKKTB];
            TimeKKTB = [TimeKKTB; tKKTB];
        end

        Time{n,m,CNR} = TimeCNR(:);
        Time{n,m,HIL} = TimeHIL(:);
        Time{n,m,KKTB} = TimeKKTB(:);
        Success{n,m,CNR} = SuccessCNR(:);
        Success{n,m,HIL} = SuccessHIL(:);
        Success{n,m,KKTB} = SuccessKKTB(:);
        
        
        %Del = [];
        TimeCNR = [];
        SuccessCNR = [];
        %Residual = [];
        %Deg = [];
        
        TimeHIL = [];
        SuccessHIL = [];
        TimeKKTB = [];
        SuccessKKTB = [];
    end
end

for n=3
    for m=n:4
        for k=1:50
            pathname = p{n,m};
            
            % loading JAC results
            filename = strcat('Results_JAC(',int2str(n),int2str(m),')',int2str(k),'.mat');
            data = fullfile(pathname, filename);
            load(data)
            SuccessJAC = [SuccessJAC; testJAC];
            TimeJAC = [TimeJAC; tJAC];
        end
        Time{n,m,JAC} = TimeJAC(:);
        Success{n,m,JAC} = SuccessJAC(:);
        
        TimeJAC = [];
        SuccessJAC = [];
    end
end

disp("Data Stored, computing averages")

avgT = {};
avgS = {};
for n=3
    for m=n:5
        avgT{n,m,CNR} = mean(Time{n,m,CNR},1);
        avgS{n,m,CNR} = mean(Success{n,m,CNR},1);
        
        avgT{n,m,HIL} = mean(Time{n,m,HIL},1);
        avgS{n,m,HIL} = mean(Success{n,m,HIL},1);
        
        avgT{n,m,KKTB} = mean(Time{n,m,KKTB},1);
        avgS{n,m,KKTB} = mean(Success{n,m,KKTB},1);
    end
end

for n=3
    for m=n:4
        avgT{n,m,JAC} = mean(Time{n,m,JAC},1);
        avgS{n,m,JAC} = mean(Success{n,m,JAC},1);
    end
end

disp("Plotting results")

for n=3
    for m=n:4
        figure();
        hold on
        plot((1:1:50), cumsum(Time{n,m,CNR}'), 'b-')
        plot((1:1:50), cumsum(Time{n,m,HIL}'), 'r-')
        plot((1:1:50), cumsum(Time{n,m,KKTB}'),'k-')
        plot((1:1:50), cumsum(Time{n,m,JAC}'), 'm-')
        xlabel('trial')
        ylabel('Cumulative computation time (s)')
        title(strcat('Problems of size (',int2str(n),',',int2str(m),')'))
        legendCell = {'CNR','HIL','KKTB','JAC'};
        legend(legendCell)
        
        figure();
        hold on
        plot((1:1:50), (1/50)*cumsum(Success{n,m,CNR}'), 'b-')
        plot((1:1:50), (1/50)*cumsum(Success{n,m,HIL}'), 'r-')
        plot((1:1:50), (1/50)*cumsum(Success{n,m,KKTB}'),'k-')
        plot((1:1:50), (1/50)*cumsum(Success{n,m,JAC}'), 'm-')
        xlabel('trial')
        ylabel('Cumulative successes rate (%)')
        ylim([0,1])
        yticks([0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
        yticklabels({0 10 20 30 40 50 60 70 80 90 100})
        title(strcat('Problems of size (',int2str(n),',',int2str(m),')'))
        legendCell = {'CNR','HIL','KKTB','JAC'};
        legend(legendCell)
    end
end

figure();
hold on
plot((1:1:50), cumsum(Time{3,5,CNR}'), 'b-')
plot((1:1:50), cumsum(Time{3,5,HIL}'), 'r-')
plot((1:1:50), cumsum(Time{3,5,KKTB}'),'k-')
xlabel('trial')
ylabel('Cumulative computation time (s)')
title('Problems of size (3,5)')
legendCell = {'CNR','HIL','KKTB'};
legend(legendCell)

figure();
hold on
plot((1:1:50), (1/50)*cumsum(Success{3,5,CNR}'), 'b-')
plot((1:1:50), (1/50)*cumsum(Success{3,5,HIL}'), 'r-')
plot((1:1:50), (1/50)*cumsum(Success{3,5,KKTB}'),'k-')
xlabel('trial')
ylabel('Cumulative successes rate (%)')
ylim([0,1])
yticks([0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
yticklabels({0 10 20 30 40 50 60 70 80 90 100})
title('Problems of size (3,5)')
legendCell = {'CNR','HIL','KKTB'};
legend(legendCell)
