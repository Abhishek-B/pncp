function [ del ] = step3_KKT(n,m,v,V,d)
% step3_KKT - Computes a SOS decomposition of the final form in Algorithm 4.1
%             using the KKT variety.
%
% Inputs:
%        n,m - Integers which determine the size of the spaces 
%              used.
%          v - Vector that defines the non-SOS form 'f'.
%              NOTE: v should be computed by Step2_2.
%          V - Matrix whose columns define the linear forms 'h_{i}'.
%              NOTE: V should be computed using 'step1_2' and 'step1_3'
%              together.
%          d - Integer upper bound on the degree of the decision
%              polynomials 'phi' and 'eta'.  
%
% Output:
%        del - Floating point number which determines the non-negative,
%              non-SOS form over the Segre variety.
%
% NOTE: The optimization problem is solved using YALMIP and the SDP solver
% Penlab. Other solvers are possible with YALMIP and can be implemented 
% (see the YALMIP documentation) by the user directly in the function file.
% Implementation without YALMIP is not supported.
%
% In practice this method should not be used with large degrees 'd'. If low
% degrees don't give a satisfactory solution, the problem should either be
% reformulated, or a different method used. 



%% sdpvar's

Z = sdpvar(n,m,'full');                                     % variables Z in matrix
Z2 = reshape(Z', [n*m,1]);                                  % variables Z in vector
sdpvar delta;                                               % decision variable delta

lambda = sdpvar((nchoosek(n,2))*(nchoosek(m,2)),1);         % KKT multipliers

monlist = monolist([Z2;lambda],d);                          % monomials for polynomial multipliers
coeffgrad = sdpvar(n*m, size(monlist,1),'full');            % coefficients of multipliers of gradients
coeffcons = sdpvar(size(lambda,1), size(monlist,1),'full'); % coefficients of multipliers of constraints

etavec = coeffgrad*monlist;                                 % multipliers of gradients
phivec = coeffcons*monlist;                                 % multipliers of constraints

%% decision variables

coeffgrad2 = reshape(coeffgrad, [(size(coeffgrad,1))*(size(coeffgrad,2)),1]);
coeffcons2 = reshape(coeffcons, [(size(coeffcons,1))*(size(coeffcons,2)),1]);

decvar = [delta; lambda; coeffgrad2; coeffcons2];


%% linear and quadratic forms

f = v'*(kron(Z2,Z2));                                       % quad form from step 2
h = V'*Z2;                                                  % linear forms from step 1

F = delta*f + (10)*(h'*h);                                 % positive non-sos form

%% Variety constrainsts
constraint = [];                                               % vector of segre variety constraints

for i=1:n-1
    for k=i+1:n
        for j=1:m-1
            for l=j+1:m
                g = Z(i,j)*Z(k,l) - Z(k,j)*Z(i,l);          % 2x2 minors of Z
                constraint = [constraint; g];
            end
        end
    end
end
%% KKT constraints

gradG = jacobian(constraint, Z2)';                          % columns are gradients of variet constraints
gradF = jacobian(F, Z2)';                                   % gradient of F
gradients = gradF + gradG*lambda;                           % sum of gradients for KKT
%% sos program

poly = F - etavec'*gradients - phivec'*constraint;
prog = sos(poly);


%% sdp solver

ops = sdpsettings('solver', 'penlab');
ops.verbose = 0;



[sol, u, Q]=solvesos(prog, -delta, ops, decvar);           % Running optimizer
if sol.problem==0
    disp('SOS decomposition is possible.')
    del = value(delta);
else 
    disp('SOS decomposition not found.')
    del = 0;
end







%{
solvesos(prog, -delta, ops, decvar);
soln = value(decvar);

del = soln(1);
%}
end



