function [Output] = Ent_PnCP(M, N, m, state, attempts)
% PCPentchk - A matlab function to test the entanglement of a state by
% applying the general criterion using PnCP maps.
%
% Abhishek Bhardwaj 2019.
%
% Inputs: 
%            M - Integer; dimension of subsystem A of state.
%            N - Integer; dimension of subsystem B of state.
%            m - Integer; dimension of the image of Phi (Phi:S^{N}->S^{m})
%        state - Matric; density matrix represting quantum state to be
%                tested.
%     attempts - Integer; upper bound on the number of different Phi to test  
%
% Outputs: (1x2 cell containing the following elements)
%
%          Phi - PnCP map which is negative on the state.
% Mapped_state - The mapping Phi applied to state.
%


%% Initialize Entanglement

Entanglement = 0;

%% determine the size of the state
[nrow, ncol] = size(state);

if nrow~=ncol
    error("State matrix should be square")
end
% if ~issymmetric(state)
%     error("State matrix needs to be symmetric")
% end


trial = 1;
% building function Fdelta
while trial<=attempts
    disp("Trial number")
    disp(trial)
    n = N;
%    m = 3;
    
    nng = 0;
    count = 0;
    % creating form
    while nng==0 && count<20
        ss = 1;
        disp("building function")
        while ss==1
            [X,Y,Z] = Rstep1_1(n,m);
            V = Rstep1_2(n,m,Z);
            v0 = Rstep1_3(n,m,Z);
            W = Rstep2_1(n,m,Z);
            v = Rstep2_2(n,m, Z, W, v0, V);
            [ss, del] = isSOS(n,m,v,[v0 V],'mosek');
        end
        disp("testing nng")
        % testing NNG
        [nng, G, res] = Rstep3HilbertB(n,m,v,[v0 V],2,1,'mosek');
        count = count+1;
        disp(count)
    end
    disp("finished")
    if nng==1
        % SDP vars to extract Phi from Fdelta
        disp('Extracting Phi')
        X = sdpvar(n,1);
        Y = sdpvar(m,1);
        Z = kron(X,Y);
        f = double(v')*kron(Z,Z);                                             % defining the non-sos quad form f
        h = double(V')*Z;                                                     % The linear forms h constructed in step 1
        
        F = f + (h'*h);
        
        [C, V1] = coefficients(F, X);
        
        % defining operator Phi
        
        Phi = cell(n,n);
        
        symmat = X*X';
        for i=1:n
            for j=i:n
                
                for k=1:length(V1)
                    
                    if class(V1(k)-symmat(i,j))=='double'
                        
                        poly = C(k);
                        Q = sdpvar(m,m,'symmetric');
                        SOL = [coefficients(poly-Y'*Q*Y, Y) == 0];
                        optimize(SOL,[], sdpsettings('verbose',0));
                        G = value(Q);
                        if j==i
                            Phi{i,j} = G;
                        elseif j~=i
                            Phi{i,j} = 0.5*G;
                        end
                    end
                end
            end
        end
        
        disp('Extending Phi')
        
        for i=2:n
            for j=1:i-1
                Phi{i,j} = Phi{j,i};
            end
        end
        
        % Splitting state into blocks of given size
        
        vec = N*ones(M,1);
        
        bstate = mat2cell(state, vec,vec);
        %disp(bstate)
        % Checking Phi negative on state
        
        disp('Applying Phi to state')
        %disp(bstate)
        Mapped_state = cell(M,M);
        %disp("Mapped State")
        %disp(Mapped_state)
        for i=1:M
            for j=1:M
                %disp([n,m])
                L = bstate{i,j};
                %disp("bstate{i,j}")
                %disp(L)
                [Lrows, Lcols] = size(L);
                Lmap = zeros(m,m);
                %disp("Lmap")
                %disp(Lmap)
                for k=1:Lrows
                    for l=1:Lcols
                        Lmap = Lmap + L(k,l)*Phi{k,l};
                    end
                end
                Mapped_state{i,j} = Lmap;
            end
        end
        %disp(Mapped_state)
        
        Mapped_state = cell2mat(Mapped_state);
        
        disp('Checking the mapped state')
        if any(eig(Mapped_state)<-1e-8)
            disp("non-negative map on state found")
            Output = {Phi, Mapped_state};
            break
        else
            disp("Mapped state is positive semi-definite")
        end
        trial = trial+1;
    elseif nng==0
        disp('Non-negative map not found')
        trial = trial+1;
    end
end


if trial==attempts+1 || attempts<0
    disp('Attempts limit reached without finding successful entanglement map')
    Output = {};
end
































end

