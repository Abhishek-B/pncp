
%% creating bound entangled state given in the article


    
I4 = eye(4);
I3 = eye(3);

% Building the state vectors given in the article

v1 = (1/sqrt(2))*( kron( I4(:,1), (I3(:,1)-I3(:,2)) ) );
v2 = (1/sqrt(2))*( kron( I4(:,2), (I3(:,2)-I3(:,3)) ) );
v3 = (1/sqrt(2))*( kron( I4(:,3), (I3(:,3)-I3(:,1)) ) );

v4 = (1/sqrt(2))*( kron( (I4(:,2)-I4(:,4)), I3(:,1) ) );
v5 = (1/sqrt(2))*( kron( (I4(:,3)-I4(:,4)), I3(:,2) ) );
v6 = (1/sqrt(2))*( kron( (I4(:,1)-I4(:,4)), I3(:,3) ) );

v7 = (1/(2*sqrt(3)))*( kron( (I4(:,1)+I4(:,2)+I4(:,3)+I4(:,4)), (I3(:,1)+I3(:,2)+I3(:,3)) ) );

% defining the state sigma

sigma = (1/5)*( eye(12) - (v1*v1'+ v2*v2'+ v3*v3'+ v4*v4'+ v5*v5'+ v6*v6'+ v7*v7'));

%% Running PnCP Entagnlement Check

tic;
Output = Ent_PnCP(4,3,3,sigma,3);
Phi = Output{1,1};
M = Output{1,2};
timetaken = toc;

%% We check again this time increasing Phi's mapped dimension to 4

% tic;
% Output2 = PCPentchk(4,3,4,sigma,3);
% Phi2 = Output2{1,1};
% M2 = Output2{1,2};
% timetaken2 = toc;





