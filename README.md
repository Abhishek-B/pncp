# PnCP 
## A Matlab package for quantum entanglement detection and constructing positive maps which are not completely positive. 

To learn about what PnCP does and to see some performance standards, please read - https://arxiv.org/abs/2001.01181  
If you use PnCP, please cite the above article for its use.  
To use PnCP, add the entire matlab archive to path, and see the 'demo.m' file.

PnCP is released under the conditions of the [GNU General Public License 3.0](https://www.gnu.org/licenses/gpl-3.0.html)

If you find bugs in the package, please report to Abhishek.Bhardwaj@anu.edu.au.
Thank you.